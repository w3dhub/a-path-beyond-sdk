@ECHO off
goto check_Permissions

:check_Permissions
    net session >nul 2>&1
    if NOT %errorLevel% == 0 (
        echo You must run this batch file as an administrator.
		pause
		goto :fail
    )

set /p MammothPath=Enter the path to your preferred Mammoth executable (include quotation marks if the path has spaces): 

@ASSOC .lvl=W3DMammoth.LevelFile
@FTYPE W3DMammoth.LevelFile=%MammothPath% --open-level "%%1"

:fail