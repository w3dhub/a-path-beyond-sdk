macroscript ImportW3D
	category: "W3D Importer"
	buttontext: "Import W3D"
	tooltip: "Import a W3D file"
	icon:#("RenXWME",1)
(
	OpenW3DImporter()
)

macroscript SetW3DFlags
	category: "W3D Importer"
	buttontext: "Set W3D Flags"
	tooltip: "Set W3D tool flags"
	icon:#("RenXWME",2)
(
	OpenW3DFlagsTool()
)
