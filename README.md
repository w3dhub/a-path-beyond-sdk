Red Alert: A Path Beyond SDK
===

This SDK contains tools and assets you will need to create maps and mods for Red Alert: A Path Beyond.
For in depth information about using these tools or for any other questions please visit
[our website](http://w3dhub.com)

This SDK includes the following tools;

Mammoth
---
A level editor designed to work with maps for the W3D engine. 
It is similar to the now-defunct LevelEdit tool by Westwood and has been extended to support new functionality available in the latest version of scripts.dll.

Before you can use Mammoth you must have a copy of Red Alert: A Path Beyond installed. If your APB game client is not located at the default installation path, you must correct it in mammothpath.ini.



